<?php

namespace AppBundle\DataFixtures\ORM;





use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdminFixture implements FixtureInterface, ORMFixtureInterface
{

    private $encoder;



    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     * @throws \Doctrine\Common\DataFixtures\BadMethodCallException
     */


    public function load(ObjectManager $manager)
    {



        $user = new User();

        $password = $this->encoder->encodePassword($user,'123');
        $user->setRoles(array('ROLE_ADMIN'))
            ->setEnabled(true)
            ->setPassword($password)
            ->setEmail('admin@mail.ru')
            ->setUsername('admin');

        $manager->persist($user);

        $manager->flush();
    }


}