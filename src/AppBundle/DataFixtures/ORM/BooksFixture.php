<?php
namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Book;
use AppBundle\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class BooksFixture extends Fixture implements DependentFixtureInterface
{


    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     * @throws \Doctrine\Common\DataFixtures\BadMethodCallException
     */


    public function load(ObjectManager $manager)
    {

        /** @var Category $category1 */
        $category1 = $this->getReference(CategoriesFixture::CATEGORY1);
        /** @var Category $category2 */
        $category2 = $this->getReference(CategoriesFixture::CATEGORY2);
        /** @var Category $category3 */
        $category3 = $this->getReference(CategoriesFixture::CATEGORY3);


        $book = new Book();

        $book->setName('Книга 1')
            ->setAuthor('Дубровсикй - какой то')
            ->setPicture('88064444.jpg')
            ->addCategories($category1);

        $manager->persist($book);


        $book = new Book();

        $book->setName('Книга 2')
            ->setAuthor('Жевотноводский какой то')
            ->setPicture('88064444.jpg')
            ->addCategories($category3);

        $manager->persist($book);


        $book = new Book();

        $book->setName('Книга 3')
            ->setAuthor('Крутой какой то')
            ->setPicture('88064444.jpg')
            ->addCategories($category2);

        $manager->persist($book);

        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    function getDependencies()
    {
        return [
            CategoriesFixture::class
        ];
    }

}