<?php

namespace AppBundle\DataFixtures\ORM;




use AppBundle\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class CategoriesFixture extends Fixture
{

    public const CATEGORY1 = 'Ужасы';
    public const CATEGORY2 = 'Комедии';
    public const CATEGORY3 = 'Триллеры';

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     * @throws \Doctrine\Common\DataFixtures\BadMethodCallException
     */


    public function load(ObjectManager $manager)
    {

        $category = new Category();
        $category
            ->setCategory(self::CATEGORY1);

        $manager->persist($category);
        $this->addReference(self::CATEGORY1, $category);

        $category = new Category();
        $category
            ->setCategory(self::CATEGORY2);

        $manager->persist($category);
        $this->addReference(self::CATEGORY2, $category);

        $category = new Category();
        $category
            ->setCategory(self::CATEGORY3);
        $manager->persist($category);
        $this->addReference(self::CATEGORY3, $category);

        $manager->flush();
    }

}