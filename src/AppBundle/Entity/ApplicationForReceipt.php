<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ApplicationForReceipt
 *
 * @ORM\Table(name="application_for_receipt")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ApplicationForReceiptRepository")
 */
class ApplicationForReceipt
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var string
     *
     */
    private $number_ticket;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="return_date", type="datetime")
     */
    private $return_date;

    /**
     * @var  Book
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Book", inversedBy="application")
     */
    private $book;

    /**
     * @var Reader
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Reader", inversedBy="application")
     */
    private $reader;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set returnDate
     *
     * @param \DateTime $returnDate
     *
     * @return ApplicationForReceipt
     */
    public function setReturnDate($returnDate)
    {
        $this->return_date = $returnDate;

        return $this;
    }

    /**
     * Get returnDate
     *
     * @return \DateTime
     */
    public function getReturnDate()
    {
        return $this->return_date;
    }

    /**
     * @return Book
     */
    public function getBook()
    {
        return $this->book;
    }

    /**
     * @param Book $book
     */
    public function setBook($book)
    {
        $this->book = $book;
    }

    /**
     * @return LibraryCard
     */
    public function getReader()
    {
        return $this->reader;
    }

    /**
     * @param LibraryCard $reader
     */
    public function setReader($reader)
    {
        $this->reader = $reader;
    }

    /**
     * @return string
     */
    public function getNumberticket()
    {
        return $this->number_ticket;
    }

    /**
     * @param string $number_ticket
     */
    public function setNumberticket($number_ticket)
    {
        $this->number_ticket = $number_ticket;
    }
}

