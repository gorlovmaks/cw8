<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Reader
 *
 * @ORM\Table(name="Reader")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReaderRepository")
 */
class Reader
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="fullname", type="string", length=127)
     */
    private $fullname;

    /**
     * @var string
     *
     * @ORM\Column(name="adress", type="text")
     */
    private $adress;

    /**
     * @var string
     *
     * @ORM\Column(name="passport_id", type="string", length=127, unique=true)
     */
    private $passportID;


    /**
     * @var ApplicationForReceipt
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\ApplicationForReceipt", mappedBy="reader")
     */
    private $application;

    /**
     * @var string
     *
     * @ORM\Column(name="number_ticket", type="string", length=127)
     */
    private $number_ticket;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fullname
     *
     * @param string $fullname
     *
     * @return Reader
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;

        return $this;
    }

    /**
     * Get fullname
     *
     * @return string
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * Set adress
     *
     * @param string $adress
     *
     * @return Reader
     */
    public function setAdress($adress)
    {
        $this->adress = $adress;

        return $this;
    }

    /**
     * Get adress
     *
     * @return string
     */
    public function getAdress()
    {
        return $this->adress;
    }

    /**
     * Set passportID
     *
     * @param string $passportID
     *
     * @return Reader
     */
    public function setPassportID($passportID)
    {
        $this->passportID = $passportID;

        return $this;
    }

    /**
     * Get passportID
     *
     * @return string
     */
    public function getPassportID()
    {
        return $this->passportID;
    }

    /**
     * @return ApplicationForReceipt
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * @param Reader $application
     */
    public function setApplication($application)
    {
        $this->application = $application;
    }

    /**
     * @return string
     */
    public function getNumberTicket(): string
    {
        return $this->number_ticket;
    }

    /**
     * @param string $number_ticket
     */
    public function setNumberTicket(string $number_ticket)
    {
        $this->number_ticket = $number_ticket;
    }


}

