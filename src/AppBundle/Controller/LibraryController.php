<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\ApplicationForReceipt;
use AppBundle\Entity\Category;
use AppBundle\Entity\Reader;
use AppBundle\Form\LibraryCardForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class LibraryController extends Controller
{
    /**
     * @Route("/")
     */
    public function index()
    {

        $books = $this->getDoctrine()->getRepository('AppBundle:Book')->findAll();

        /** @var  Category $categories */
        $categories = $this->getDoctrine()->getRepository('AppBundle:Category')->findAll();

        return $this->render('@App/Library/library.html.twig', array(
            'books' => $books,
            'categories' => $categories
        ));
    }

    /**
     * @Route("/category/{id}", requirements={"id": "\d+"})
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function filtrationCategory(int $id)
    {

        $category = $this->getDoctrine()->getRepository('AppBundle:Category')->find($id);

        $books = $category->getBooks();
        return $this->render('@App/Library/categoryList.html.twig', array(
            'books' => $books,
        ));
    }

    /**
     * @Route("/newTicket")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newLibraryCard(Request $request){

        $codeTicket = rand(1,1000000);

        $ticket = new Reader();

        $form = $this->createForm(
            LibraryCardForm::class,
            $ticket);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $ticket->setNumberTicket($codeTicket);
            $em = $this->getDoctrine()->getManager();
            $em->persist($ticket);
            $em->flush();
            return $this->redirectToRoute('app_library_index');
        }

        return $this->render('@App/Library/newTicket.html.twig', array(
            'form' => $form->createView(),
            'code' => $codeTicket
        ));
    }



    /**
     * @Route("/{_locale}/", requirements = {"_locale" : "en|ru"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function locale(Request $request){

        return $this->redirect($request->headers->get('referer'));
    }

}
